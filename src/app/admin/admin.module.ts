import {NgModule} from '@angular/core';
import {appAdminRoutingProviders, routingAdmin} from './admin.routing';
import {TagService} from './services/tag.service';
import {AdminTagListComponent} from './components/tag-list/tag-list.component';
import {AdminTagListItemComponent} from "app/admin/components/tag-list/item/item.component";
import {AdminIndexComponent} from "./components/index/index.component";
import { AdminTagFormComponent } from './components/tag-list/tag-form/tag-form.component';
import {
    MdButtonModule, MdCardModule, MdCheckboxModule, MdIconModule, MdInputModule, MdListModule, MdSidenavModule,
    MdToolbarModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [
        CommonModule,
        MdButtonModule,
        MdInputModule,
        MdCardModule,
        MdCheckboxModule,
        MdIconModule,
        MdToolbarModule,
        MdSidenavModule,
        MdListModule,
        ReactiveFormsModule,
        FormsModule,
        routingAdmin
    ],
    providers: [
        appAdminRoutingProviders,
        TagService
    ],
    declarations: [
        AdminIndexComponent,
        AdminTagListComponent,
        AdminTagListItemComponent,
        AdminTagFormComponent]
})
export class AdminModule {
}
