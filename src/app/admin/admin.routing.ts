import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminTagListComponent} from './components/tag-list/tag-list.component';
import {AdminIndexComponent} from './components/index/index.component';
import {AdminTagListItemComponent} from './components/tag-list/item/item.component';


const appAdminRoutes: Routes = [
    {
        path: 'admin', component: AdminIndexComponent,
        children: [
            {
                path: 'tags', component: AdminTagListComponent,
                children: [
                    {path: ':id', component: AdminTagListItemComponent},
                ]
            },
        ]
    },


];

export const appAdminRoutingProviders: any[] = [];

export const routingAdmin: ModuleWithProviders = RouterModule.forRoot(appAdminRoutes);