import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTagListItemComponent } from './item.component';

describe('AdminTagListItemComponent', () => {
  let component: AdminTagListItemComponent;
  let fixture: ComponentFixture<AdminTagListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTagListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTagListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
