import {Component, Input, OnInit} from '@angular/core';
import {Tag} from "../../../models/tag";
import {TagService} from "../../../services/tag.service";
import {pr} from "../../../../../environments/debug";

@Component({
    selector: 'app-admin-tag-list-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss']
})
export class AdminTagListItemComponent {


    @Input() tag: Tag;

    constructor() {
    }

}
