import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTagFormComponent } from './tag-form.component';

describe('AdminTagFormComponent', () => {
  let component: AdminTagFormComponent;
  let fixture: ComponentFixture<AdminTagFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTagFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTagFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
