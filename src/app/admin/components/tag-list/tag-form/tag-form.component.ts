import {Component, OnInit} from '@angular/core';
import {TagForm} from "../../../forms/tag-form";
import {FormGroup} from "@angular/forms";
import {TagService} from "../../../services/tag.service";
import {pr} from "../../../../../environments/debug";

@Component({
    selector: 'app-admin-tag-form',
    templateUrl: './tag-form.component.html',
    styleUrls: ['./tag-form.component.sass']
})
export class AdminTagFormComponent implements OnInit {
    private tagForm: FormGroup;
    private submitted: boolean = false;

    constructor(private tagService: TagService) {
    }

    ngOnInit() {
        this.tagForm = TagForm.get();

    }

    save(formValue: any, isValid: boolean) {
        this.submitted = true;
        if (isValid) {
            this.tagService.createTag(formValue);
            this.submitted = false;
            this.tagForm.reset();

        }

    }
}
