import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTagListComponent } from './tag-list.component';

describe('AdminTagListComponent', () => {
  let component: AdminTagListComponent;
  let fixture: ComponentFixture<AdminTagListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTagListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTagListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
