import {Component, OnInit} from '@angular/core';
import {TagService} from '../../services/tag.service';
import {pr} from '../../../../environments/debug';
import * as Rx from 'rxjs';


@Component({
    selector: 'app-admin-tag-list',
    templateUrl: './tag-list.component.html',
    styleUrls: ['./tag-list.component.scss']
})
export class AdminTagListComponent implements OnInit {
    query: any;
    tags: any;

    constructor(private tagService: TagService) {
    }

    ngOnInit() {
        this.tags = this.tagService.tags;
        const demoInput = document.querySelector('#query');
        Rx.Observable.fromEvent(demoInput, 'keyup')
            .map(x => this.query)
            .filter((v) => {
                return v.length > 2;
            })
            .debounceTime(500)
            .subscribe(
                x => {
                    pr(x);
                }
            );
    }


}
