import {FormBuilder, FormGroup, Validators} from '@angular/forms';

export class TagForm {
    static get(): FormGroup {
        return (new FormBuilder()).group({
            title: ['', [
                <any>Validators.required,
                <any>Validators.minLength(5),
            ]],
            status: ['', []],
        });
    }
}
