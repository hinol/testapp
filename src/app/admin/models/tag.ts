export class Tag {
    get status(): boolean {
        return this._status;
    }

    set status(value: boolean) {
        this._status = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    private _title: string;
    private _status: boolean;


    public constructor(data) {
        if (data.title) {
            this.title = data.title;
        }
        if (data.status) {
            this.status = data.status;
        }
    }


    static create(data) {
        return new Tag(data);
    }


}
