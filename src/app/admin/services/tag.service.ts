///<reference path="../../../../node_modules/rxjs/BehaviorSubject.d.ts"/>
import {EventEmitter, Injectable} from '@angular/core';
import {Tag} from "../models/tag";
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import {pr} from "../../../environments/debug";
import * as Rx from 'rxjs';

/**
 * https://coryrylan.com/blog/angular-observable-data-services
 */


@Injectable()
export class TagService {
    q: any;
    eventEmitter: EventEmitter<any> = new EventEmitter();


    query: any;
    private _tags: BehaviorSubject<Tag[]>;
    private dataStore: {
        tags: Tag[]
    };

    constructor() {
        this._tags = <BehaviorSubject<Tag[]>>new BehaviorSubject([]);
        this.dataStore = {
            tags: []
        };

    }

    get tags(): Rx.Observable<Tag[]> {
        return this._tags.asObservable();
    }


    createTag(_data: any) {

        Rx.Observable.timer(0, 1000)
            .take(1)
            .subscribe(
                data => {
                    const tag = Tag.create(_data);
                    this.dataStore.tags.push(tag);
                    this._tags.next(Object.assign({}, this.dataStore).tags);
                },
            )
    }


}
