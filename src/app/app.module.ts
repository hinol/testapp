import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {appRoutingProviders, routing} from './app.routing';
import {MdButtonModule} from '@angular/material';
import 'hammerjs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AdminModule} from './admin/admin.module';
import {StoreModule} from '@ngrx/store';
import {tagReducer} from './common/reducers/tagReduer';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        AdminModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        BrowserAnimationsModule,
        MdButtonModule,
        routing,
        StoreModule.provideStore({ tag: tagReducer })

    ],
    providers: [
        appRoutingProviders
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
