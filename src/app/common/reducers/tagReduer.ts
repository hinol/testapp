// counter.ts
import { ActionReducer, Action } from '@ngrx/store';

export const QUERY = 'QUERY';
export const ADD = 'ADD';

export function tagReducer(state: number = 0, action: Action) {
    switch (action.type) {
        case QUERY:
            return state + 1;

        case ADD:
            return state - 1;


        default:
            return state;
    }
}